from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_meshviewer/django_cdstack_tpl_meshviewer"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    yanic_services = str()

    for key in template_opts.keys():
        if key.startswith("yanic_profile_") and key.endswith("_name"):
            key_ident = key[:-5]

            yanic_profile = dict()

            yanic_profile["yanic_domain_name"] = template_opts[key_ident + "_name"]
            yanic_profile["yanic_interface"] = template_opts[key_ident + "_interface"]

            if key_ident + "_influxdb_enable" in template_opts:
                if template_opts[key_ident + "_influxdb_enable"] == "true":
                    yanic_profile["yanic_influxdb_enable"] = template_opts[
                        key_ident + "_influxdb_enable"
                    ]
                    yanic_profile["yanic_influxdb_address"] = template_opts[
                        key_ident + "_influxdb_address"
                    ]
                    yanic_profile["yanic_influxdb_port"] = template_opts[
                        key_ident + "_influxdb_port"
                    ]

                    if key_ident + "_influxdb_username" in template_opts:
                        yanic_profile["yanic_influxdb_username"] = template_opts[
                            key_ident + "_influxdb_username"
                        ]
                        yanic_profile["yanic_influxdb_password"] = template_opts[
                            key_ident + "_influxdb_password"
                        ]

            config_template_file = open(
                module_prefix + "/templates/config-fs/dynamic/etc/yanic/domain.conf",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/yanic/" + yanic_profile["yanic_domain_name"] + ".conf",
                config_template.render(yanic_profile),
            )

            yanic_services += (
                "systemctl enable yanic@" + yanic_profile["yanic_domain_name"] + "\n"
            )
            yanic_services += (
                "mkdir -p /var/lib/yanic/"
                + yanic_profile["yanic_domain_name"]
                + "/public"
                + "\n"
            )

    template_opts["yanic_services"] = yanic_services

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
